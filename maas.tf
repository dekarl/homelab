#provider "maas" {
#  api_key = data.sops_file.secrets.data["maas.api_key"]
#  api_url = "http://192.168.66.254:5240/MAAS"
#}


#
# automatic tags based on physical memory
#
resource "maas_tag" "tag-4gb" {
  name = "4gb"
  comment = "Nodes with exactly 4GiB system memory"
  definition = "//node[@id=\"memory\"]/size = \"4294967296\""
}

resource "maas_tag" "tag-8gb" {
  name = "8gb"
  comment = "Nodes with exactly 8GiB system memory"
  definition = "//node[@id=\"memory\"]/size = \"8589934592\""
}

resource "maas_tag" "tag-64gb" {
  name = "64gb"
  comment = "Nodes with exactly 64GiB system memory"
  definition = "//node[@id=\"memory\"]/size = \"68719476736\""
}

#
# manual tags to help HP t630 to actually use the available memory
# when adding 64GiB get's only 4GiB detected by the Linux kernel
#
# see also: https://www.parkytowers.me.uk/thin/hp/t630/
#
resource "maas_tag" "tag-t630-64gb" {
  name = "t630-64gb"
  comment = "4GiB memory detected, force 64GiB"
  kernel_opts = "memmap=60G@4G"
  machines    = [
    "pgx6as",
  ]
}

#
# 4x HP t620 Thin Clients
#
#resource "maas_machine" "t620-1" {
#  hostname = "jonas"
#  pxe_mac_address = "7c:d3:0a:1c:76:17"
#  power_type = "webhook"
#  power_parameters = jsonencode({
#    power_on_uri    = "http://t620s.maas/switch/sonoff_4ch_relay_x/turn_on"
#    power_off_uri  = "http://t620s.maas/switch/sonoff_4ch_relay_x/turn_off"
#    power_query_uri= "http://t620s.maas/switch/sonoff_4ch_relay_x"
#    power_on_regex = "\"state\":\"ON\""
#    power_off_regex= "\"state\":\"OFF\""
#  })
#}

resource "maas_machine" "t620-2" {
  hostname = "keks"
  pxe_mac_address = "7c:d3:0a:1b:3c:9b"
  power_type = "webhook"
  power_parameters = jsonencode({
    power_on_uri    = "http://t620s.maas/switch/sonoff_4ch_relay_2/turn_on"
    power_off_uri  = "http://t620s.maas/switch/sonoff_4ch_relay_2/turn_off"
    power_query_uri= "http://t620s.maas/switch/sonoff_4ch_relay_2"
    power_on_regex = "\"state\":\"ON\""
    power_off_regex= "\"state\":\"OFF\""
  })
}

resource "maas_machine" "t620-3" {
  hostname = "sirprize"
  pxe_mac_address = "7c:d3:0a:10:18:7a"
  power_type = "webhook"
  power_parameters = jsonencode({
    power_on_uri    = "http://t620s.maas/switch/sonoff_4ch_relay_3/turn_on"
    power_off_uri  = "http://t620s.maas/switch/sonoff_4ch_relay_3/turn_off"
    power_query_uri= "http://t620s.maas/switch/sonoff_4ch_relay_3"
    power_on_regex = "\"state\":\"ON\""
    power_off_regex= "\"state\":\"OFF\""
  })
}

resource "maas_machine" "t620-4" {
  hostname = "stupsi"
  pxe_mac_address = "7c:d3:0a:1d:99:20"
  power_type = "webhook"
  power_parameters = jsonencode({
    power_on_uri    = "http://t620s.maas/switch/sonoff_4ch_relay_4/turn_on"
    power_off_uri  = "http://t620s.maas/switch/sonoff_4ch_relay_4/turn_off"
    power_query_uri= "http://t620s.maas/switch/sonoff_4ch_relay_4"
    power_on_regex = "\"state\":\"ON\""
    power_off_regex= "\"state\":\"OFF\""
  })
}

#
# 2x HP t630 Thin Clients
#
resource "maas_machine" "t630-1" {
  pxe_mac_address = "f8:b4:6a:25:25:62"
  power_type = "webhook"
  power_parameters = jsonencode({
    power_on_uri    = "http://t620s.maas/switch/sonoff_4ch_relay_1/turn_on"
    power_off_uri  = "http://t620s.maas/switch/sonoff_4ch_relay_1/turn_off"
    power_query_uri= "http://t620s.maas/switch/sonoff_4ch_relay_1"
    power_on_regex = "\"state\":\"ON\""
    power_off_regex= "\"state\":\"OFF\""
  })
}

#resource "maas_machine" "t630-2" {
#  pxe_mac_address = ""
#  power_type = "webhook"
#  power_parameters = jsonencode({
#    power_on_uri    = "http://t620s.maas/switch/sonoff_4ch_relay_x/turn_on"
#    power_off_uri  = "http://t620s.maas/switch/sonoff_4ch_relay_x/turn_off"
#    power_query_uri= "http://t620s.maas/switch/sonoff_4ch_relay_x"
#    power_on_regex = "\"state\":\"ON\""
#    power_off_regex= "\"state\":\"OFF\""
#  })
#}
