provider "unifi" {
  username = "terraform"                                   // optionally use UNIFI_USERNAME env var
  password = data.sops_file.secrets.data["unifi.password"] // optionally use UNIFI_PASSWORD env var
  # IPv6 doesn't work
  api_url  = "https://unifi.spaetfruehstuecken.org/" // optionally use UNIFI_API env var
  # api_url = "https://unifi.fritz.box:8443/" // optionally use UNIFI_API env var
  # api_url  = "https://192.168.68.52:8443/" // optionally use UNIFI_API env var
  # allow_insecure = true

  // if you are not configuring the default site, you can change the site
  site = "default" // optionally use UNIFI_SITE env var
}


data "unifi_ap_group" "default" {
}

data "unifi_user_group" "default" {
}

resource "unifi_network" "default" {
  name          = "LAN"
  purpose       = "corporate"
  domain_name   = "schlosskonrad.spaetfruehstuecken.org"
  subnet        = "192.168.66.1/24"
  dhcp_enabled  = false
  igmp_snooping = true
}

resource "unifi_wlan" "wifi-daheim" {
  name         = "DaHeim"
  network_id   = unifi_network.default.id
  passphrase   = data.sops_file.secrets.data["unifi.daheim-passphrase"]
  ap_group_ids = [data.unifi_ap_group.default.id]
  user_group_id = data.unifi_user_group.default.id
  security      = "wpapsk"
  no2ghz_oui    = false
  wlan_band     = "both"
}


resource "unifi_network" "network-guest" {
  name    = "FritzGuest"
  purpose = "guest"
  subnet  = "192.168.189.1/24"
  vlan_id = 10
}

# https://www.youtube.com/watch?v=9JMQg0d7z7I&t=4m42s
resource "unifi_wlan" "wifi-liegewiese" {
  name          = "Liegewiese"
  network_id    = unifi_network.network-guest.id
  passphrase    = data.sops_file.secrets.data["unifi.guest-passphrase"]
  ap_group_ids  = [data.unifi_ap_group.default.id]
  user_group_id = data.unifi_user_group.default.id
  security   = "wpapsk"
  no2ghz_oui = false
  wlan_band  = "both"
  is_guest   = false
}


# add some networks for freifunk traffic
resource "unifi_network" "network-freifunkmesh" {
  name          = "FreifunkMesh"
  purpose       = "vlan-only"
  subnet        = "192.168.69.1/24"
  vlan_id       = 22
}

resource "unifi_network" "network-freifunk" {
  name          = "FreifunkClient"
  purpose       = "vlan-only"
  subnet        = "192.168.68.1/24"
  vlan_id       = 20
}
resource "unifi_wlan" "wifi-freifunk" {
  name          = "darmstadt.freifunk.net"
  network_id    = unifi_network.network-freifunk.id
  ap_group_ids  = [data.unifi_ap_group.default.id]
  user_group_id = data.unifi_user_group.default.id
  security      = "open"
}


resource "unifi_user" "switch-sued-2" {
  mac        = "e0:63:da:59:25:c6"
  name       = "sued-switch-2"
  network_id = unifi_network.default.id
}

resource "unifi_user" "switch-west" {
  mac        = "e0:63:da:59:4b:e2"
  name       = "west-switch"
  network_id = unifi_network.default.id
}

resource "unifi_user" "switch-west-wz" {
  mac        = "70:a7:41:c1:ba:ae"
  name       = "west-switch-wz"
  network_id = unifi_network.default.id
}

resource "unifi_user" "switch-sued" {
  mac        = "60:E3:27:E3:4C:B4"
  name       = "sued-switch"
  fixed_ip   = "192.168.66.2"
  network_id = unifi_network.default.id
}

resource "unifi_network" "network-maas" {
  name    = "MaaS"
  purpose = "vlan-only"
  subnet  = "192.168.68.1/24"
  vlan_id = 30
}

resource "unifi_network" "network-nord" {
  name    = "LAN Dennis"
  purpose = "vlan-only"
  subnet  = "192.168.178.1/24"
  vlan_id = 40
}

resource "unifi_wlan" "wifi-nord" {
  name         = "Phoenix-Nest"
  network_id   = unifi_network.network-nord.id
  passphrase   = data.sops_file.secrets.data["unifi.nord-passphrase"]
  ap_group_ids = [data.unifi_ap_group.default.id]
  user_group_id = data.unifi_user_group.default.id
  security      = "wpapsk"
  no2ghz_oui    = false
  wlan_band     = "both"
}

resource "unifi_port_profile" "profile-nord" {
  name = "nord"
  # pick the native VLAN by assigning the id of the network
  native_networkconf_id = unifi_network.network-nord.id
}

resource "unifi_device" "switch-west-2" {
  # optionally specify MAC address to skip manually importing
  # manual import is the safest way to add a device
  mac = "70:A7:41:C1:BD:9D"

  name = "west-switch-2"

  port_override {
    number          = 5
    name            = "router-nord"
    port_profile_id = unifi_port_profile.profile-nord.id
  }
}


# Setup MaaS network
resource "unifi_port_profile" "profile-maas" {
  name = "MaaS"
  # pick the native VLAN by assigning the id of the network
  native_networkconf_id = unifi_network.network-maas.id
}

#resource "unifi_device" "switch-sued-2" {
#  # optionally specify MAC address to skip manually importing
#  # manual import is the safest way to add a device
#  mac = "e0:63:da:59:25:c6"
#
#  name = "sued-switch-2"
#
#  port_override {
#    number          = 3
#    name            = "switch-maas"
#    port_profile_id = unifi_port_profile.profile-maas.id
#  }
#
#  port_override {
#    number              = 5
#    name                = "truenas"
#    op_mode             = "aggregate"
#    aggregate_num_ports = 2
#  }
#}
