provider "maas" {
  api_key = data.sops_file.secrets.data["maas.api_key"]
  api_url = "http://192.168.66.254:5240/MAAS"
}

provider "random" {
}

# generate 32 char bootstrap token (a-zA-Z0-9)
# https://kubernetes.io/docs/reference/access-authn-authz/bootstrap-tokens/

#resource "random_password" "bootstrap-token-id" {
#  length = 6
#  special = false
#  upper = false
#}
#resource "random_password" "bootstrap-token-secret" {
#  length = 16
#  special = false
#  upper = false
#}
resource "random_password" "bootstrap-token" {
  length  = 32
  special = false
  upper   = false
}
locals {
  # bootstrap-token = join (".", [random_password.bootstrap-token-id.result, random_password.bootstrap-token-secret.result])
  bootstrap-token = random_password.bootstrap-token.result
}

resource "maas_instance" "k3s_master" {
  count   = 1

#  allocate_params {
#    tags  = ["64gb"]
#  }

# NOT IMPORTED - REMOVED TO AVOID REPLACING THE INSTALL WHILE SWITCHING PROVIDERS
#  deploy_params {
#    user_data = <<-EOT
#      #!/bin/bash
#      sudo snap install microk8s --classic --channel=1.23/stable
#
#      # let "ubuntu" use kubectl without sudo
#      sudo usermod -a -G microk8s ubuntu
#      sudo chown -f -R ubuntu ~ubuntu/.kube
#      echo alias kubectl=\'microk8s kubectl\' >> ~ubuntu/.bash_aliases
#
#      # add known, but still random, bootstrap token to join nodes later
#      sudo microk8s add-node --token ${local.bootstrap-token}
#
#      # wait for cluster to come up
#      sudo microk8s status --wait-ready
#      sudo microk8s disable ha-cluster
#      sudo microk8s enable dns
#
#      # setup bootstrap runner
#      sudo microk8s enable helm3
#      echo gitlabUrl: https://gitlab.com/ > values.yaml
#      echo name: bootstrap-in-kubernetes >> values.yaml
#      echo runners: >> values.yaml
#      echo "  tags: k8s-bootstrap" >> values.yaml
#      echo runnerRegistrationToken: ${data.sops_file.secrets.data["gitlab.runnerRegistrationToken"]} >> values.yaml
#      sudo microk8s kubectl create namespace deployment
#      sudo microk8s helm3 repo add gitlab https://charts.gitlab.io
#      sudo microk8s helm3 upgrade --install --namespace deployment bootstrap-runner --values values.yaml --wait gitlab/gitlab-runner
#      rm values.yaml
#
#      #bring your own cluster to kubesail
#      #sudo microk8s kubectl create -f https://byoc.kubesail.com/dekarl.78fa2adf8e.full.yaml
#      EOT
#  }
}


resource "maas_instance" "k3s_worker" {
  count   = 0

  deploy_params {
    user_data = <<-EOT
      #!/bin/bash
      sudo snap install microk8s --classic --channel=1.23/stable

      sudo microk8s status --wait-ready

      sudo microk8s join kube-master.maas:25000/${local.bootstrap-token} --worker
      EOT
  }

  depends_on = [
    maas_instance.k3s_master,
  ]
}
