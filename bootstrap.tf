terraform {
  backend "http" {
  }

  required_providers {
    maas = {
      source  = "maas/maas"
      version = "2.1.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.4.3"
    }
    sops = {
      source  = "carlpett/sops"
      version = "~> 0.7.2"
    }
    unifi = {
      source  = "paultyng/unifi"
      version = "0.41.0"
    }
  }
}

provider "sops" {}

data "sops_file" "secrets" {
  source_file = "secrets.json"
}
