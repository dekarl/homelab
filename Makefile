TF_ROOT=/root/homelab
VOLUME_SOURCE=-v ${PWD}:/root/homelab
VOLUME_KEY=-v ${HOME}/.config/sops/age:/root/.config/sops/age:ro

TF_ADDRESS=https://gitlab.com/api/v4/projects/26192534/terraform/state/default
TF_HTTP_USERNAME=dekarl
TF_HTTP_PASSWORD=$$(sops --decrypt --extract '["gitlab"]["token"]' secrets.json)
EXPORTS=export TF_ADDRESS=${TF_ADDRESS}; \
        export TF_HTTP_USERNAME=${TF_HTTP_USERNAME}; \
        export TF_HTTP_PASSWORD=${TF_HTTP_PASSWORD};

DOCKER=${EXPORTS} docker run --rm ${VOLUME_SOURCE} ${VOLUME_KEY} --env TF_ADDRESS --env TF_HTTP_USERNAME --env TF_HTTP_PASSWORD
IMAGE=registry.gitlab.com/gitlab-org/terraform-images/stable:latest

init:
	${DOCKER} ${IMAGE} /bin/sh -c "cd ${TF_ROOT} && gitlab-terraform init -upgrade"

test-import:
#	${DOCKER} ${IMAGE} /bin/sh -c "cd ${TF_ROOT} && gitlab-terraform import unifi_network.default name=LAN"
#	${DOCKER} ${IMAGE} /bin/sh -c "cd ${TF_ROOT} && gitlab-terraform import unifi_network.network-guest name=FritzGuest"
#	${DOCKER} ${IMAGE} /bin/sh -c "cd ${TF_ROOT} && gitlab-terraform import unifi_network.network-maas name=MaaS"
#	${DOCKER} ${IMAGE} /bin/sh -c "cd ${TF_ROOT} && gitlab-terraform import unifi_network.network-nord name='LAN Michael'"

#
#
# perform imports like this:
#   ADDR=unifi_wlan.wifi-daheim ID=5fde15aa5ab20c014e3f7c66 make import
#
#
import:
	${DOCKER} ${IMAGE} /bin/sh -c "cd ${TF_ROOT} && gitlab-terraform import ${ADDR} ${ID}"

plan:
	${DOCKER} ${IMAGE} /bin/sh -c "cd ${TF_ROOT} && gitlab-terraform plan"
